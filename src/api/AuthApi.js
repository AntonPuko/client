import Base from './Base';

const ENDPOINTS = {
  signIn: `devices/auth`,
  getSmsCode: `auth`,
  validatePhone: `validate`,
  signUp: `users/register`,
};

const AUTH_STATUSES = {
  TEMP_TOKEN_STATUS: 2,
  REG_TOKEN_STATUS: 3,
};

export default class AuthApi extends Base {
  singInUser = (phone, password, udid) =>
    this.apiClient.post(ENDPOINTS.signIn, {phone, password, udid})
      .then(resp => {
        const authToken = resp.data.response.auth_token;
        const userInfo = resp.data.response.user;

        return {userInfo, authToken};
      })
      .catch(err => {
        throw new Error(err.data.error.message);
      });

  getSmsCode = (phone, udid) =>
    this.apiClient.post(ENDPOINTS.getSmsCode, {phone, udid})
      .then(resp => {
        const respStatus = resp.data.response.status;

        if (respStatus === AUTH_STATUSES.TEMP_TOKEN_STATUS) {
          const token = resp.data.response.token;
          const tokenTimeout = resp.data.response.timeout;
          return {token, tokenTimeout, temp: true};
        } else {
          const token = resp.data.response.token;
          return {token, temp: false};
        }
      })
      .catch(err => {
        throw new Error(err.data.error.message);
      });

  validateSmsCode = (code, token, udid) =>
    this.apiClient.post(ENDPOINTS.validatePhone,
      {
        token,
        code,
        udid,
      })
      .then(resp => resp.data.response.token)
      .catch(err => {
        throw new Error(err.data.error.message);
      });

  signUpUser = (userData) => {
    const formData = new FormData();
    Object.keys(userData).forEach((key) => {
      formData.append(key, userData[key]);
    });

    return this.apiClient.post(ENDPOINTS.signUp, formData)
      .then((resp) => {
        const authToken = resp.data.response.auth_token;        
        const userInfo = resp.data.response.user;
        
        return {userInfo, authToken};
      })
      .catch(err => {
        throw new Error(err.data.error.message);
      });
  }

}






