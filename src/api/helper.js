'use strict';

// костыль для добавления прокси ко всем хттп запросам
(function() {
  //var cors_api_host = 'cors-anywhere.herokuapp.com';
  //var cors_api_url = 'https://' + cors_api_host + '/';
  const local = 'localhost:8080';
  const heroku_deploy = 'yol-cors-server.herokuapp.com';
  const cors_api_host = local;
  const cors_api_url = 'https://' + heroku_deploy + '/proxy?url=';

  const slice = [].slice;
  const origin = window.location.protocol + '//' + window.location.host;
  const open = XMLHttpRequest.prototype.open;
  XMLHttpRequest.prototype.open = function() {
    const args = slice.call(arguments);
    const targetOrigin = /^https?:\/\/([^\/]+)/i.exec(args[1]);
    if (targetOrigin && targetOrigin[0].toLowerCase() !== origin &&
      targetOrigin[1] !== cors_api_host) {
      args[1] = cors_api_url + args[1];
    }
    return open.apply(this, args);
  };
})();