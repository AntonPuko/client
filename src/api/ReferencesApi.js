import Base from './Base';

const ENDPOINTS = {
  getReferences: `references`,
  getCityList: `cities_list`,
};

export default class ReferencesApi extends Base {
  fetchReferences = () => 
    this.apiClient.get(ENDPOINTS.getReferences)
      .then(resp => resp.data.response);



  getCityList = () =>
    this.apiClient.get(ENDPOINTS.getCityList)
      .then(resp => resp.data.response);
}

