import lsFactory from './localStorage/';

const localStorage = new lsFactory();

export default localStorage;