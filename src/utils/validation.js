const isEmpty = value => value === undefined || value === null || value === '';

const join = (rules) => (value, data) =>
  rules.map(rule => rule(value, data))
    .filter(error => !!error)[0 /* first error */]; //todo разобраться

export function email(value) {
  if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'неверный формат email';
  }
}

export function required(value) {
  if (isEmpty(value)) {
    return 'обязательное поле';
  }
}

export function minLength(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return `не меньше ${min} символов`;
    }
  };
}

export function match(field) {
  return (value, data) => {
    if (data) {
      if (value !== data[field]) {
        return 'не совпадает';
      }
    }
  };
}

export function createValidator(rules) {
  return (data = {}) => {
    const errors = {};

    Object.keys(rules).forEach((key) => {
      const rule = join([].concat(rules[key]));
      const error = rule(data[key], data);
      if (error) {
        errors[key] = error;
      }
    });

    return errors;
  };
}