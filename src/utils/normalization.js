'use strict';
export function normalizeNumeric(value) {
  if (!value) {
    return value
  }
  return value.replace(/[^\d]/g, '');
}


export function normalizeSmsCode(value) {
  const maxSmsCodeLength = 4;

  if (!value) {
    return value
  }

  var numeric = normalizeNumeric(value);
  if(numeric.length > maxSmsCodeLength) return numeric.slice(0,maxSmsCodeLength);

  return numeric;
}