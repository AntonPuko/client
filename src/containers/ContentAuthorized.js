import React, {Component, PropTypes} from 'react';
import BaseContentLayout from '../components/layouts/BaseContentLayout';
import SideMenu from '../containers/SideMenu';

const ContentAuthorized = (props) => (
  <BaseContentLayout
    mainContent={props.children}
    aside={<SideMenu />}
    hideMainContent={false}/>
);

ContentAuthorized.propTypes = {
  children: PropTypes.element,
};


export default ContentAuthorized;