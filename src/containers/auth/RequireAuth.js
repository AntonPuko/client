import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {ROUTES} from '../../routes';

export default function (ComposedComponent) {
  class RequireAuth extends Component {
    static propTypes = {
      authorized: PropTypes.bool.isRequired,
    };

    static contextTypes = {
      router: React.PropTypes.object,
    };

    componentWillMount() {
      if (!this.props.authorized) {
        this.context.router.push(ROUTES.SIGN_IN);
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authorized) {
        this.context.router.push(ROUTES.SIGN_IN);
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  return connect(state =>
    ({authorized: state.auth.signIn.authorized})
  )(RequireAuth);
}