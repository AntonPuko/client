import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {ROUTES} from '../../routes';

export default function (ComposedComponent) {
  class RequireUnauth extends Component {
    static propTypes = {
      authorized: PropTypes.bool.isRequired,
    };

    static contextTypes = {
      router: React.PropTypes.object,
    };

    componentWillMount() {
      if (this.props.authorized) {
        this.context.router.push(ROUTES.HOME);
      }
    }

    componentWillUpdate(nextProps) {
      if (nextProps.authorized) {
        this.context.router.push(ROUTES.HOME);
      }
    }

    render() {
      return <ComposedComponent {...this.props}/>;
    }
  }

  return connect(state =>
    ({authorized: state.auth.signIn.authorized})
  )(RequireUnauth);
}