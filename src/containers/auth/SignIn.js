import React, {Component} from 'react';

import BaseContentLayout from '../../components/layouts/BaseContentLayout';
import SignInForm from './../../components/auth/SignIn/SignInForm/SignInForm';
import SignInAdvers from './../../components/auth/SignIn/SignInAdvert/SignInAdvert';

const SignInContainer = () => (
  <BaseContentLayout
    aside={<SignInForm />}
    mainContent={<SignInAdvers />}
    hideMainContent={true}
  />
);

export default SignInContainer;