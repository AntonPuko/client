import React, {Component, PropTypes} from 'react';

import {connect} from 'react-redux';
import {getSmsCode, validateSmsCode, signUpUser} from '../../redux/actions/authActions';

import BaseContentLayout from '../../components/layouts/BaseContentLayout';
import SignInForm from '../../components/auth/SignIn/SignInForm/SignInForm';
import PhoneVerificationForm
  from './../../components/auth/SignUp/PhoneVerificationForm/PhoneVerificationForm';
import RegistrationForm from './../../components/auth/SignUp/RegistrationForm/RegistrationForm';

const SignUpContainer = (props) => {
  const form = props.auth.signUp.phoneVerified ?
    <RegistrationForm
      auth={props.auth}
      signUpUser={props.signUpUser}
    /> :
    <PhoneVerificationForm
      auth={props.auth}
      getSmsCode={props.getSmsCode}
      validateSmsCode={props.validateSmsCode}
    />;

  return (
    <BaseContentLayout
      aside={<SignInForm />}
      mainContent={form}
      hideMainContent={false}/>
  );
};

SignUpContainer.propTypes = {
  auth: PropTypes.object.isRequired,
  getSmsCode: PropTypes.func.isRequired,
  validateSmsCode: PropTypes.func.isRequired,
  signUpUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({auth: state.auth});
export default connect(mapStateToProps, {getSmsCode, validateSmsCode, signUpUser})(SignUpContainer);
