import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {
  fetchComments,
  createComment,
  likeComment,
  unlikeComment,
} from '../redux/actions/postsActions';

import CommentExpandButton from '../components/CommentBox/CommentExpandButton';
import CommentList from '../components/CommentBox/CommentList';
import CommentForm from '../components/CommentBox/CommentForm';

import CircularProgress from '../components/common/CircularProgress';
import Divider from 'material-ui/Divider';

@connect(null, {fetchComments, createComment, likeComment, unlikeComment})
export default class CommentBoxContainer extends Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    fetchComments: PropTypes.func.isRequired,
    createComment: PropTypes.func.isRequired,
    likeComment: PropTypes.func.isRequired,
    unlikeComment: PropTypes.func.isRequired,
    fetchSize: PropTypes.number,
    expandCount: PropTypes.number,
  };

  static defaultProps = {
    expandCount: 3,
    fetchSize: 25
  };

  state = {
    expanded: false,
  };

  componentDidMount() {
    if (this.props.post.comments.size === 0 && this.props.post.comments_count > 0) {
      this.props.fetchComments(this.props.post.id, this.props.post.lastCommentId);
    }
  }

  handleExpandClick = () => {
    if (this.state.expanded &&
      this.props.post.comments.size < this.props.post.comments_count) {
      this.props.fetchComments(this.props.post.id, this.props.post.lastCommentId);
      this.setState({expanded: true});
    } else {
      this.setState({expanded: !this.state.expanded});
    }
  };

  renderExpandBtn(comments) {
    if (comments.size > this.props.expandCount) {
      return (
        <CommentExpandButton
          onClick={this.handleExpandClick}
          expanded={this.state.expanded}
          fetchSize={this.props.fetchSize}
          fetchedCommentsCount={comments.size}
          allCommentsCount={this.props.post.comments_count}
        />
      );
    }
  }

  render() {
    const comments = this.props.post.comments.toList().sortBy(c => c.created_at);

    const renderComments = this.state.expanded ? comments : comments.slice(-this.props.expandCount);

    // render loading progress if there're comments but no fetched yet
    if (comments.size === 0 && this.props.post.comments_count > 0) {
      return <CircularProgress size={1}/>;
    }

    return (
      <div>
        {this.renderExpandBtn(comments)}
        <CommentList
          comments={renderComments}
          postId={this.props.post.id}
        />
        <Divider />
        <CommentForm
          createComment={this.props.createComment.bind(this, this.props.post.id)}
          formKey={this.props.post.id.toString()}
        />
      </div>
    );
  }
}

