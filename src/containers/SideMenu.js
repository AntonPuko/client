import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {signOutUser} from '../redux/actions/authActions';

import SideMenu from '../components/SideMenu';

@connect(null, {signOutUser})
export default class SideMenuContainer extends Component {
  static propTypes = {
    signOutUser: PropTypes.func.isRequired,
  }

  static contextTypes = {
    router: React.PropTypes.object,
  };

  handleMenuValueChange = (e, value) => {
    if (value) {
      this.context.router.push(value);
    }
  }

  handleExitTouch = () => {
    this.props.signOutUser();
  }
  
  render() {
    return (
      <SideMenu
        onExitTouch={this.handleExitTouch}
        onMenuValueChange={this.handleMenuValueChange}
      />
    );
  }
}