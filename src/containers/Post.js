import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {likePost, unlikePost} from '../redux/actions/postsActions';
import Post from '../components/posts/Post';
import _ from 'lodash';

@connect(null, {likePost, unlikePost})
export default class PostContainer extends Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    likePost: PropTypes.func.isRequired,
    unlikePost: PropTypes.func.isRequired,
  };   
  
  state = {
    showCommentBox: false,
  };

  sayhello() {
    console.log('hello');
  }
  
  handleLikePostClick = () => {

    _.throttle(this.sayhello.bind(this),200);

    if (!this.props.post.liked) {
      this.props.likePost(this.props.post.id);
    } else {
      this.props.unlikePost(this.props.post.id);
    }
  };
  
  handleCommentClick = () => {
    this.setState({showCommentBox: !this.state.showCommentBox});
  };  
  
  render() {
    return (
      <Post
        post={this.props.post}
        showCommentBox={this.state.showCommentBox}
        onLikePostClick={this.handleLikePostClick}
        onCommentClick={this.handleCommentClick}
      />
    );
  }
}