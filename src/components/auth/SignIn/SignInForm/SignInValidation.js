import * as v from '../../../../utils/validation';

const signUpValidation = v.createValidator({
  phone: [v.required],
  password: [v.required, v.minLength(6)],
});

export default signUpValidation;