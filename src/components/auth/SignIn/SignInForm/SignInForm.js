import React, {Component, PropTypes} from 'react';

import {reduxForm} from 'redux-form';
import signUpValidation from './SignInValidation';
import {signInUser} from '../../../../redux/actions/authActions';
import styles from './SignInForm.scss';

import {Link} from 'react-router';
import {ROUTES} from '../../../../routes';

import FormErrorAlert from '../../../common/FormErrorAlert';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';

const FORM_OPTIONS = {
  form: 'signIn',
  fields: ['phone', 'password'],
  validate: signUpValidation,
};

@reduxForm(FORM_OPTIONS, null, {signInUser})
export default class SignInForm extends Component {
  static propTypes = {
    signInUser: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    error: PropTypes.string,
    fields: PropTypes.object,
  };

  handleSubmitForm({phone, password}) {
    return this.props.signInUser({phone, password})
      // server validation errors
      .catch((err) => Promise.reject({_error: err.message}));
  }

  render() {
    const {
      handleSubmit, submitting, error,
      fields: {phone, password},
    } = this.props;

    return (
      <Paper zDepth={1} className={styles.box}>
        <form onSubmit={handleSubmit(this.handleSubmitForm.bind(this))}>
          <TextField
            {...phone}
            hintText="Телефон"
            floatingLabelText='Телефон'
            className={styles.textField}
            errorText={phone.touched && phone.error ? phone.error : ''}
          />
          <TextField
            {...password}
            hintText="Пароль"
            floatingLabelText="Пароль"
            className={styles.textField}
            type="password"
            errorText={password.touched && password.error ? password.error : ''}
          />
          <fieldset className={styles.formGroupBox}>
            <RaisedButton
              className={styles.button}
              label="Войти"
              primary={true}
              disabled={submitting}
              type="submit"
            />
          </fieldset>
          <fieldset className={styles.formGroupBox}>
            <Link to={ROUTES.SIGN_UP}>
              <RaisedButton
                className={styles.button}
                label="регистрация"
                primary={true}
              />
            </Link>
          </fieldset>
          <div className={styles.formGroupBox}>
            {error ? <FormErrorAlert error={error}/> : ''}
            <Link to={ROUTES.PASS_RECOVERY}>Забыли пароль?</Link>
          </div>
        </form>
      </Paper>
    );
  }
}

