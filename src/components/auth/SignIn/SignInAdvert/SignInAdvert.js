import React, {PropTypes} from 'react';
import styles from './SignInAdvert.scss';

import iosImg from './img/1.png';
import androidImg from './img/2.png';

import Paper from 'material-ui/Paper';
import InstallAppAdvert from './InstallAppAdvert/InstallAppAdvert';

const SHOP_LINKS = {
  ios: 'https://itunes.apple.com/us/app/yol-social-naa-set-ucastnikov/id1067410184?l=ru',
  android: 'https://play.google.com/store/apps/details?id=ru.appkode.yol',
};

const SignInAdvert = () => (
  <Paper zDepth={1} className={styles.box}>
    <div className="row">
      <h4 className="text-xs-center">Установите наше мобильное приложение!</h4>
    </div>
    <div className="row">
      <div className="col-sm-6">
        <InstallAppAdvert
          imgSrc={iosImg}
          storeLinkText="ios appstore"
          storeLink={SHOP_LINKS.ios}
        />
      </div>
      <div className="col-sm-6">
        <InstallAppAdvert
          imgSrc={androidImg}
          storeLinkText="android play market"
          storeLink={SHOP_LINKS.android}
        />
      </div>
    </div>
  </Paper>
);

export default SignInAdvert;