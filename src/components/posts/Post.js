import React, {Component, PropTypes} from 'react';
import styles from './Post.scss';
import moment from 'moment';

import {Link} from 'react-router';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';

import CommentBox from './../../containers/CommentBox';
import Attachment from '../Attachment/Attachment';

export default class Post extends Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    onLikePostClick: PropTypes.func,
    onCommentClick: PropTypes.func,
    showCommentBox: PropTypes.bool,
  };

  renderPostHeader() {
    return (
      <div className={styles.header}>
        <Avatar
          src={this.props.post.user.img.thumb}
          size={70}
        />
        <div className={styles.headerTitle}>
          <div className={styles.headerUsername}>
            {this.props.post.user.name}
            <span className={styles.userId}> id:{this.props.post.user.id}</span>
          </div>
          <div className={styles.headerDate}>
            {moment.unix(this.props.post.created_at).fromNow()}
          </div>
        </div>
        <div className={styles.headerEllipsisBox}>
          <IconButton iconClassName={`fa fa-ellipsis-h ${styles.ellipsisIconBtn}`}/>
        </div>
      </div>
    );
  }

  renderPostContent() {
    return (
      <div>
        <div className={styles.contentText}>
          {this.props.post.text}
        </div>
        <Attachment attachments={this.props.post.attachments}/>
      </div>
    );
  }

  renderPostFooter() {
    //todo link magic string
    return (
      <div className={styles.footer}>
        <div className={styles.footerCategory}>
          <Link to={`/feed/categories/${this.props.post.category.id}`}>
            {this.props.post.category.title}
          </Link>
        </div>
        <div className={styles.footerComment}>
          <IconButton
            iconClassName={`fa fa-comment ${styles.commentIconBtn}`}
            onClick={this.props.onCommentClick}
          /> {this.props.post.comments_count}
        </div>
        <div className={styles.footerLikes}>
          <IconButton
            iconClassName={`fa fa-heart  ${this.props.post.liked ?
              styles.likeIconBtn_liked : styles.likeIconBtn}`}
            onClick={this.props.onLikePostClick}
          /> {this.props.post.likes}
        </div>
      </div>
    );
  }

  render() {
    return (
      <Paper zDepth={1} className={styles.teaserBox}>
        {this.renderPostHeader()}
        {this.renderPostContent()}
        {this.renderPostFooter()}
        {this.props.showCommentBox ? <CommentBox post={this.props.post}/> : null}
      </Paper>
    );
  }
}
