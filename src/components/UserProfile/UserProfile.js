import React, {Component, PropTypes} from 'react';
import styles from './UserProfile.scss';

import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';

export default class UserProfile extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
  }

  render() {
    const user = this.props.user;
    console.log(user);
    return (

      <Paper className={styles.box} zDepth={1}>
        <div className="row">
          <div className={`col-md-4 ${styles.avatarBox}`}>
            <Avatar src={user.img.middle} className={styles.avatar}/>
            <RaisedButton label="изменить"/>
          </div>
          <div className={`col-md-8 ${styles.userInfoBox}`}>
            <div>
              <span>id:</span> {user.id}
            </div>
            <div>
              <span>Имя:</span> {user.name}
            </div>
            <div>
              <span>Телефон:</span> {user.phone.number}
            </div>
            <div>
              <span>дата рождения:</span> {user.birthday}
            </div>
            <div>
              <span>город:</span> {user.cityName}
            </div>
          </div>
          <div>
            <RaisedButton label="добавить машину" />
          </div>
        </div>
      </Paper>
    );
  }
}
