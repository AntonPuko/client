import React, {Component, PropTypes} from 'react';
import styles from './Attachment.scss';
import Slider from 'react-slick';

const ATTACH_TYPES = {
  image: 'Image',
  video: 'Video',
};
//todo container and own component for every attach type
export default class Attachment extends Component {

  static propTypes = {
    attachments: PropTypes.array,
  }

  renderAttachItem(item) {
    switch (item.type) {
      case ATTACH_TYPES.image:
        return (
          <div key={item.id} className={styles.imageBox}>
            <img className={styles.image} src={item.main}/>
          </div>
        );
      case ATTACH_TYPES.video:
        return (
          <div key={item.id} className={styles.videoBox}>
            <iframe
              className={styles.video}
              src={`https://www.youtube.com/v/${item.video_ref}`}
              frameBorder="0"
              allowFullScreen
              seamless="seamless"></iframe>
          </div>
        );
      default:
        return <div key={item.id}>*unhandled attachment type*</div>; // todo
    }
  }

  renderSliderItems(items) {
    return items.map((item) => {
      return this.renderAttachItem(item);
    });
  }

  render() {
    const attachments = this.props.attachments;
    if (attachments.length === 0) return null;
    else if (attachments.length === 1) {
      return this.renderAttachItem(attachments[0]);
    }
    else {
      return (
        <Slider dots={true}>
          {this.renderSliderItems(attachments)}
        </Slider>
      );
    }
  }
}
