import React, {Component, PropTypes} from 'react';
import styles from './Comment.scss';
import moment from 'moment';

import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';

const Comment = ({comment, onLikeComment}) => (   
  <div className={comment.isCreating ? styles.commentBox_creating : styles.commentBox}>
    
    <Avatar src={comment.user.img.thumb}/>
    
    <div className={styles.contentBox}>
      <div className={styles.contentHeader}>
        <div className={styles.name}>
          {comment.user.name} <span className={styles.id}>id: {comment.user.id}</span>
        </div>
      </div>
      <div className={styles.content}>
        <div>{comment.text}</div>
      </div>
      
      <div className={styles.footer}>
        <div className={styles.date}>
          {moment.unix(comment.created_at).fromNow()}
        </div>
        <div className={styles.answer}>
        </div>
        <div className={styles.likesBox}>
          <IconButton
            style={{height: '6px', width: '6px'}}
            iconStyle={{top: '-5px', left: '-6px'}}
            iconClassName={`fa fa-heart  ${comment.liked ?
              styles.likeIconBtn_liked : styles.likeIconBtn}`}
            onClick={onLikeComment}
          />
          <div className={styles.likeText}>
            {comment.likes}
          </div>
        </div>
      </div>
      
    </div>
    
  </div>
);

Comment.propTypes = {
  comment: PropTypes.object.isRequired, // todo раписать shape
  onLikeComment: PropTypes.func,
};

export default Comment;