import React, {Component, PropTypes} from 'react';
import styles from './CommentForm.scss';
import FlatButton from 'material-ui/FlatButton';
import Textarea from 'react-textarea-autosize';

import {reduxForm} from 'redux-form';

// todo refactor to lib
function validate(formProps) {
  const errors = {};

  if (!formProps.comment) {
    errors.comment = 'введите сообщение';
  }

  return errors;
}

const FORM_OPTIONS = {
  form: 'postCommentForm',
  fields: ['comment'],
  validate,
};

@reduxForm(FORM_OPTIONS)
export default class CommentForm extends Component {
  static propTypes = {
    createComment: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
  };

  handleSubmitForm = ({comment}) => {
    console.log('here2');
    this.props.resetForm();
    return this.props.createComment(comment);
  };

  handleEnterPress = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.handleSubmitForm({comment: this.props.fields.comment.value}); //todo КОСТЫЛЬ!!!!
    }
  };

  render() {
    const {handleSubmit, fields: {comment}} = this.props;

    return (
      <form
        className={styles.formBox}
        onSubmit={handleSubmit(this.handleSubmitForm)}>
        <Textarea
          className={styles.textarea}
          placeholder="написать комментарий..." {...comment}
          onKeyPress={this.handleEnterPress}
        />
        <div className={styles.footer}>
          <FlatButton
            ref="btn"
            type="submit"
            label="отрпавить"
          />
        </div>
      </form>
    );
  }
}