'use strict';
import React, {Component} from 'react';
import DatePicker from 'material-ui/DatePicker';
import moment from 'moment';

class DatePickerWrapper extends Component {
  onChange(evt, date) { 
    if (this.props.onChange) {
      this.props.onChange(date);
    }
  }

  onDismiss() {
    if (this.props.value) {
      this.props.onChange(null);
    }
  }

  render() {
    return (<DatePicker 
      {...this.props}
      onChange={this.onChange.bind(this)}
      onDismiss={this.onDismiss.bind(this)}
    />);
  }
}

export default DatePickerWrapper;