import React, {PropTypes} from 'react';
import styles from './CircularProgressWrapper.scss';
import CircularProgress from 'material-ui/CircularProgress';

const CircularProgressWrapper = (props) => (
  <div className={styles.loadingBox}>
    <CircularProgress size={props.size}/>
  </div>
);

CircularProgressWrapper.propTypes = {
  size: PropTypes.number,
};

CircularProgressWrapper.defaultProps = {
  size: 1,
};

export default CircularProgressWrapper;