import React, {Component, PropTypes} from 'react';
import styles from './MaxWidthWrapper.scss';

const MaxWidthWrapper = ({children}) => (
  <div className={styles.box}>
    {children}
  </div>
);

MaxWidthWrapper.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MaxWidthWrapper;