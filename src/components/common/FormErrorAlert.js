import React, {PropTypes} from 'react';

const FormErrorAlert = ({error}) => {
  return (
    <div className="alert alert-danger">
      <strong>Oops!</strong> {error}
    </div>
  );
};

FormErrorAlert.propTypes = {
  error: PropTypes.string,
};

export default FormErrorAlert;