import React, {Component} from 'react';
import {connect} from 'react-redux';
import {signOutUser} from '../../../redux/actions/authActions';
import styles from './HeaderAuthorized.scss';

import {red500} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import Tabs from 'material-ui/Tabs/Tabs';
import Tab from 'material-ui/Tabs/Tab'
import FontIcon from 'material-ui/FontIcon';

import {ROUTES} from '../../../routes';
//todo useless component
class HeaderAuthorized extends Component {
  static contextTypes = {
    router: React.PropTypes.object,
  };


  onSignOutButtonClick() {
    this.props.signOutUser();
  }

  handleActivate(tab) {
    this.context.router.push(tab.props.value);
  }

  render() {
    console.log(this.props.pathname);
    return (
      <div className={styles.box}>
        <Tabs value={this.props.pathname}
              className={styles.tabs}
        >
          <Tab
            icon={<FontIcon className="fa fa-file-text" />}
            label="Feed"
            className={styles.tab}
            onActive={this.handleActivate.bind(this)}
            value={ROUTES.FEED}
          >
            </Tab>
          <Tab
            label="Settings"
            className={styles.tab}
            icon={<FontIcon className="fa fa-cog" />}
            onActive={this.handleActivate.bind(this)}
            value={ROUTES.PASS_RECOVERY}
          >
          </Tab>

        </Tabs>
        <IconButton
          iconClassName="fa fa-times"
          iconStyle={{color: red500}}
          onClick={this.onSignOutButtonClick.bind(this)}
        />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    userInfo: state.user.userInfo
  }
}

export default connect(mapStateToProps, {signOutUser})(HeaderAuthorized);