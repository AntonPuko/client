import React, {Component, PropTypes} from 'react';
import styles from './App.scss';

import MaxWidthWrapper from './common/MaxWidthWrapper/MaxWidthWrapper';
import Header from './Header/Header';
import Footer from './Footer/Footer';

import {connect} from 'react-redux';
// import {fetchReferences} from '../redux/actions/referencesActions';

// todo container

// @connect(null, {fetchReferences})
class App extends Component {
  static propTypes = {
   // fetchReferences: PropTypes.func.isRequired,
    children: PropTypes.element,
  };

  componentWillMount() {
  //  this.props.fetchReferences();
  }

  render() {
    return (
      <div className={styles.box}>
        <div className={styles.header}>
          <Header pathname={this.props.location.pathname}/>
        </div>
        <div className={styles.content}>
          <MaxWidthWrapper>
            {this.props.children}
          </MaxWidthWrapper>
        </div>
        <div className={`hidden-sm-down ${styles.footer}`}>
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;