import React from 'react';
import {Route, Redirect} from 'react-router';

import App from './components/App';
import NotFoundPage from './components/NotFoundPage';
import RequireAuth from './containers/auth/RequireAuth';
import RequireUnAuth from './containers/auth/RequireUnauth';

import SignIn from './containers/auth/SignIn';
import SignUp from './containers/auth/SIgnUp';
import ContentAuthorized from './containers/ContentAuthorized';

import Test from './components/Test';

import PostFeed from './containers/PostFeed';
import UserProfile from './containers/UserProfile';
import Messages from './containers/Messages';

export const ROUTES = {
  HOME: '/',
  SIGN_IN: '/signin',
  SIGN_UP: '/signup',
  PASS_RECOVERY: '/passrecovery',
  FEED: '/feed',
  FEED_CATEGORY: '/feed/categories/:categoryId',
  USER_PROFILE: '/userprofile',
  MESSAGES: '/messages',
};

export default (
  <Route component={App}>
    <Redirect from={ROUTES.HOME} to={ROUTES.FEED}/>
    <Route path={ROUTES.HOME} component={RequireAuth(ContentAuthorized)}>
      <Route path={ROUTES.FEED} component={PostFeed}/>
      <Route path={ROUTES.FEED_CATEGORY} component={PostFeed}/>
      <Route path={ROUTES.USER_PROFILE} component={UserProfile} />
      <Route path={ROUTES.MESSAGES} component={Messages} />
    </Route>

    <Route path={ROUTES.SIGN_IN} component={RequireUnAuth(SignIn)}/>
    <Route path={ROUTES.SIGN_UP} component={RequireUnAuth(SignUp)}/>
    <Route path="test" component={Test}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);