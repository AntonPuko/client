import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, browserHistory} from 'react-router';
import routes from './routes';
import configureStore from './redux/store/configureStore';

import locStorage from './localStorageSingelton';
import {TYPES as AUTH_TYPES} from './redux/actions/authActions';
import {getSelfUserInfo} from './redux/actions/userActions';

import {TYPES as REF_TYPES, fetchReferences} from './redux/actions/referencesActions';

import './sharedStyles/core.scss';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin(); // need for material ui lib
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import API from './apiSingleton';

// todo запихать это куда нить отдельно в init
const language = window.navigator.userLanguage || window.navigator.language; //todo refactor
API.setLocale(language);

const store = configureStore();

const authToken = locStorage.getItem(locStorage.KEYS.AUTH_TOKEN);

if (authToken) {
  API.setAuthToken(authToken);
  store.dispatch({type: AUTH_TYPES.SIGN_IN.AUTH_USER, authToken});
  store.dispatch(getSelfUserInfo());
}

const references = locStorage.getItem(locStorage.KEYS.REFERENCES);
if (references) {
  store.dispatch({type: REF_TYPES.FETCH_REFERENCES, references, offline: true});
} else {
  store.dispatch(fetchReferences());
}

render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme()}>
      <Router history={browserHistory} routes={routes}/>
    </MuiThemeProvider>
  </Provider>, document.getElementById('app')
);
