import {Record, Map} from 'immutable';

const User = Record({
  id: null,
  name: '',
  email: '',
  phone: {},
  phones: Map(),
  age: null,
  birthday: '',
  city: null,
  img: {},
  sex: null,
  about: '',
  car: {},
  cars: Map(),
  completed: null,
  updated_at: null,
});

export default User;