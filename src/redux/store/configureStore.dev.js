'use strict';
import {createStore, compose, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import reduxThunk from 'redux-thunk';

export default function configureStore(initialState) {
  let store = createStore(rootReducer, initialState, compose(
    applyMiddleware(reduxThunk),
    //add redux middlewares here:
    window.devToolsExtension ? window.devToolsExtension() : f => f //add support for Redux dev tools
  ));

  if(module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers/index').default;
      store.replaceReducer(nextReducer);
    })
  }

  return store;
}