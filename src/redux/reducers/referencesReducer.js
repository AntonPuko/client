import {Record} from 'immutable';
import locStorage from '../../localStorageSingelton';
import {TYPES} from '../actions/referencesActions';

const InitialState = Record({
  models: [],
  marks: [],
  cities: [],
  vehicleTypes: [],
  bodyTypes: [],
  carColors: [],
  date: null,
});

const initialState = new InitialState;

export default function referencesReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_REFERENCES:
      if (!action.offline) {
        locStorage.setItem(locStorage.KEYS.REFERENCES, action.references);
      }

      // not new record because of wrong action.reference names (with - )
      return state
        .set('models', action.references.models)
        .set('marks', action.references.marks)
        .set('vehicleTypes', action.references['vehicle-types'])
        .set('bodyTypes', action.references['body-types'])
        .set('carColors', action.references['car-colors'])
        .set('cities', action.references.cities)
        .set('date', action.references.date);
  }

  return state;
}