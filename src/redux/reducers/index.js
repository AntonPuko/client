import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import optimist from 'redux-optimist';

import auth from './authReducer';
import user from './userReducer';
import feed from './postsReducer';
import test from './testReducer';
import references from './referencesReducer';

import * as normalization from '../../utils/normalization';

// normalization here due https://github.com/erikras/redux-form/issues/644
const rootReducer = optimist(combineReducers({
  form: form.normalize({
    signIn: {
      phone: normalization.normalizeNumeric,
    },
    phoneVerification: {
      phone: normalization.normalizeNumeric,
      smsCode: normalization.normalizeSmsCode,
    },
  }),
  auth,
  user,
  feed,
  references,
  test,
}));

export default rootReducer;