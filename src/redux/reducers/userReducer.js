import User from '../models/user';
import {TYPES} from '../actions/userActions';
import {TYPES as AUTH_TYPES } from '../actions/authActions';

const initialState = new User;

export default function (state = initialState, action) {
  switch (action.type) {
    case TYPES.GET_SELF_USER_INFO:
      return new User(action.user);
    case TYPES.SET_PRIMARY_USER_INFO:
      return state
        .set('id', action.userInfo.id)
        .set('name', action.userInfo.name)
        .set('sex', action.userInfo.sex)
        .set('img', action.userInfo.img);
    case AUTH_TYPES.SIGN_IN.UNAUTH_USER:
      return initialState;
  }
  return state;
}

