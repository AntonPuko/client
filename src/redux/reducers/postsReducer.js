import {Map, Record} from 'immutable';

import {TYPES} from '../actions/postsActions';
import {TYPES as AUTH_TYPES} from '../actions/authActions';
import Post from '../models/post';
import Comment from '../models/comment';

const InitialState = Record({
  isFetching: false,
  posts: Map(),
  lastPostId: null,
});

const initialState = new InitialState;

export default function feedReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.START_POSTS_FETCHING:
      return state.set('isFetching', true);
    case TYPES.STOP_POSTS_FETCHING:
      return state.set('isFetching', false);
    case TYPES.FETCH_POSTS:
    {
      const postsMap = action.posts.reduce((pMap, jsonPost) =>
        pMap.set(jsonPost.id, new Post(jsonPost)), Map());
      const lastPostId = action.posts[action.posts.length - 1].id;

      return state
        .set('lastPostId', lastPostId)
        .update('posts', posts => posts.merge(postsMap));
    }
    case TYPES.CLEAR_POSTS:
      return initialState;
    case TYPES.FETCH_COMMENTS:
    {
      const commentsMap = action.comments.reduce((cMap, jsonComment) =>
        cMap.set(jsonComment.id, new Comment(jsonComment)), Map());
      const lastCommentId = action.comments[action.comments.length - 1].id;

      return state
        .setIn(['posts', action.postId, 'lastCommentId'], lastCommentId)
        .updateIn(['posts', action.postId, 'comments'], comments =>
          comments.merge(commentsMap));
    }
    case TYPES.CREATE_COMMENT:
      return state
        .updateIn(['posts', action.postId, 'comments_count'], count => count + 1)
        .updateIn(['posts', action.postId, 'comments'], comments =>
          comments.set(action.tempComment.id, new Comment(action.tempComment)));
    case TYPES.CREATE_COMMENT_COMPLETE:
      return state
        .updateIn(['posts', action.postId, 'comments'], comments =>
          comments
            .delete(action.tempCommentId)
            .set(action.comment.id, new Comment(action.comment)));  
    case TYPES.LIKE_POST:
  
      return state
        .setIn(['posts', action.postId, 'liked'], 1)
        .updateIn(['posts', action.postId, 'likes'], likes => likes + 1);
    case TYPES.UNLIKE_POST:
      return state
        .setIn(['posts', action.postId, 'liked'], 0)
        .updateIn(['posts', action.postId, 'likes'], likes => likes - 1);
    case TYPES.LIKE_COMMENT:  
      return state
        .setIn(['posts', action.postId, 'comments', action.commentId, 'liked'], 1)
        .updateIn(['posts', action.postId, 'comments', action.commentId, 'likes'],
          likes => likes + 1);
    case TYPES.UNLIKE_COMMENT:  
      return state
        .setIn(['posts', action.postId, 'comments', action.commentId, 'liked'], 0)
        .updateIn(['posts', action.postId, 'comments', action.commentId, 'likes'],
          likes => likes - 1);
    case AUTH_TYPES.SIGN_IN.UNAUTH_USER:
      return initialState;
  }

  return state;
}
