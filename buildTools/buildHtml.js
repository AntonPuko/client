'use strict';
// This script copies /src/index.html into /dist/index.html
import fs from 'fs';
import colors from 'colors';
import cheerio from 'cheerio';

fs.readFile('src/index.html', 'utf8', (err,markup) => {
  if(err) {
    return console.log(err.red);
  }
  
  const $ = cheerio.load(markup);
  
  $('head').append('<link rel="stylesheet" href="styles.css">');

  fs.writeFile('dist/index.html', $.html(), 'utf8', function (err) {
    if (err) {
      return console.log(err.red);
    }
    console.log('index.html written to /dist'.green);
  });
});