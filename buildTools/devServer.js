'use strict';
import browserSync from 'browser-sync';

import historyApiFallback from 'connect-history-api-fallback';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../webpack.config.dev.js';

const bundler  = webpack(config);

browserSync({
  server: {
    baseDir: 'src',
    middleware: [
      webpackDevMiddleware(bundler, {
        // Dev middleware can't access config, so we provide publicPath
        publicPath: config.output.publicPath,
        stats: {colors: true},
        // Set to false to display a list of each file that is being bundled
        noInfo: true
      }),
      webpackHotMiddleware(bundler),
      historyApiFallback()
    ]
  },

  files: [
    'src/*.html',
    'src/*.js'
  ]
});